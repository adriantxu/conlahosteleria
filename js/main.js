/**
 * Randomiza un array
 * 
 * @param {Array} arr
 * 
 * @returns {Array}
 */
function randomizeArray(arr) {
    var currentIndex = arr.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = arr[currentIndex];
        arr[currentIndex] = arr[randomIndex];
        arr[randomIndex] = temporaryValue;
    }

    return arr;
}
;

function loadData() {
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        url: 'json/data.json'
    })
            .done(function (data) {
                try {
                    $('#items').addClass('row');

                    data = randomizeArray(data);

                    var setLightbox = false;
                    var item, images, tags, envio, social, totalImages = '';
                    var uniqueTags = [];

                    $.each(data, function (i, value) {
                        item = images = tags = envio = social = '';

                        // Images
                        if (0 < value.images.length) {
                            setLightbox = true;

                            images = '<div class="item-images">';

                            $.each(value.images, function (j, image) {
                                totalImages = '';

                                if (0 === j) {
                                    totalImages = ' data-total-images="' + value.images.length + '"';
                                }

                                images += '    <a class="venobox" href="' + image + '" data-gall="' + value.title + '"' + totalImages + '><img src="' + image + '" alt="" title="" /></a>';
                            });

                            images += '</div>';
                        }

                        // Tags
                        if (0 < value.tags.length) {
                            tags = '<div class="item-tags col-12">';
                            tags += '    <ul>';

                            $.each(value.tags, function (j, tag) {
                                tags += '       <li data-tag="' + tag + '">' + tag + '</li>';

                                if ($.inArray(tag, uniqueTags) === -1) {
                                    uniqueTags.push(tag);
                                }
                            });

                            tags += '   </ul>';
                            tags += '</div>';
                        }

                        // Envio
                        envio = '<div class="item-envio col-12">';
                        envio += '    <div class="row">';
                        envio += '        <div class="col-6">';
                        envio += '            Envío a domicilio: ' + ((true === value.envios.domicilio) ? '<i class="fa fa-thumbs-up text-success" style="font-size: 30px;"></i>' : '<i class="fa fa-thumbs-down text-danger" style="font-size: 30px;"></i>');
                        envio += '        </div>';
                        envio += '        <div class="col-6">';
                        envio += '            Recogida en local: ' + ((true === value.envios.recogida) ? '<i class="fa fa-thumbs-up text-success" style="font-size: 30px;"></i>' : '<i class="fa fa-thumbs-down text-danger" style="font-size: 30px;"></i>');
                        envio += '        </div>';
                        envio += '    </div>';
                        envio += '</div>';

                        // Social
                        // Facebook
                        if ($.inArray(value.social.facebook, ['', undefined, null]) === -1) {
                            social += ' <li class="social-facebook"><a href="' + value.social.facebook + '" title="Facebook" target="_blank"><img src="img/fb-bw.png" alt="Facebook" /></a></li>';
                        }

                        // Instagram
                        if ($.inArray(value.social.instagram, ['', undefined, null]) === -1) {
                            social += '<li class="social-instagram"><a href="' + value.social.instagram + '" title="Instagram" target="_blank"><img src="img/ig-bw.png" alt="Instagram" /></a></li>';
                        }

                        // if social
                        if ('' !== social) {
                            social = '<ul>' + social + '</ul>';
                        }

                        item = '<div class="item-wrapper col-md-4">';
                        item += '   <div class="item">';
                        item += '      <div class="item-header">';
                        item += '           <h2 class="item-title">' + value.title + '</h2>';
                        item += '       </div> <!-- /item-title -->';
                        item += '      <div class="item-body row">';
                        item += '           ' + images;
                        item += '           ' + tags;
                        item += '           ' + envio;
                        item += '      </div> <!-- /item-body -->';
                        item += '       <div class="item-footer">';
                        item += '          <div class="item-social">';
                        item += '               ' + social;
                        item += '           </div> <!-- /item-social -->';
                        item += '       </div> <!-- /item-footer -->';
                        item += '   </div> <!-- /item -->';
                        item += '</div> <!-- /item-wrapper -->';

                        $('#items').append(item);
                    });

                    // Si algun item tiene images hay que inicializar el plugin
                    if (setLightbox) {
                        setLightBox();
                    }

                    // Si existe alguna tag mostramos el titulo del bloque, la tag Todas y el listado de tags unicas
                    if (0 < uniqueTags.length) {
                        setUniqueTags(uniqueTags);
                    }

                    $('html, body').removeAttr('style');
                } catch (e) {
                    alert('Error: ' + e);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert('Error: ' + textStatus);
            })
            .always(function (data) {
                $('#wrapper').removeClass('wrapper-loading');
            })
            ;
}
;

/**
 * Muestra un listado unico de tags
 * 
 * @param {Array} tags
 */
function setUniqueTags(tags) {
    var tagsList = '';

    if (tags && 0 < tags.length) {
        tagsList = '<h2 class="text-center">Tags</h2>';
        tagsList += '<ul id="tags-list-container">';
        tagsList += '    <li title="Todas" data-tag="all">Todas</li>';

        // Randomizamos los tags
        tags = randomizeArray(tags);

        $.each(tags, function (i, tag) {
            tagsList += '    <li title="' + tag + '" data-tag="' + tag + '">' + tag + '</li>';
        });

        tagsList += '</ul>';
    }

    $('#tags-list').html(tagsList);
}
;

/**
 * Muestra/Oculta los items que tengan/no tengan la tag clicada
 * 
 * El boton all vuelve a mostrar todas las tags
 */
function filterByTag() {
    var tagClick = null;
    var tagFind = false;
    var hideItem = '';

    $('#tags-list').on('click', 'li', function () {
        // Cada vez que pinchemos en una nueva tag mostramos todas
        $('#items > .item-wrapper').removeClass('hide-item');

        tagClick = $(this).attr('data-tag');

        if ('all' === tagClick) {
            $('#tags-list-container li').removeClass('tag-active');
        } else {
            if ($(this).hasClass('tag-active')) {
                $(this).removeClass('tag-active');

                hideItem = '.hide-item';
            } else {
                $(this).addClass('tag-active');

                hideItem = '';
            }

            // Ocultamos lis item que no tengan la tag clicada3
            $('#items .item-wrapper' + hideItem).each(function (index) {
                tagFind = false;

                $(this).find('.item-tags li').each(function (index) {
                    if (tagClick === $(this).attr('data-tag')) {
                        tagFind = true;

                        return false;
                    }
                });

                if (!tagFind) {
                    if ('' === hideItem) {
                        $(this).addClass('hide-item');
                    } else {
                        $(this).removeClass('hide-item');
                    }
                }
            });
        }
    });
}
;

/**
 * Inicializacion del plugin de imagenes
 * 
 * @see https://veno.es/venobox/
 */
function setLightBox() {
    $('.venobox').venobox({
        infinigall: true,
        numeratio: true
    });
}
;

$(document).ready(function () {
    loadData();

    filterByTag();
});